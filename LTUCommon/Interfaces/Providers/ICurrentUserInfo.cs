﻿using System.Security;

namespace LTUCommon.Interfaces.Providers
{
    /// <summary>
    /// ICurrentUserInfo Interface
    /// </summary>
    public interface ICurrentUserInfo
    {
        bool HasSavedCredentials();

        void SetLoginCredentials
        (
            string userId,
            SecureString password
        );

        void ClearLoginCredentials();
        string GetUserId();
        SecureString GetPassword();
    }
}
