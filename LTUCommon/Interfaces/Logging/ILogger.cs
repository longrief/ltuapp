﻿using System;

namespace LTUCommon.Interfaces.Logging
{
    /// <summary>
    /// Ilogger Interface
    /// </summary>
    public interface ILogger
    {
        void Log(string message);
        void Log(string message, object obj);
        void Log(string message, TimeSpan duration);
        void Log(string message, TimeSpan duration, object obj);
        void Log(string message, Exception exception);
        void Log(string message, TimeSpan duration, Exception exception);
    }
}
