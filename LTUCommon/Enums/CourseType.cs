﻿namespace LTUCommon.Enums
{
    /// <summary>
    /// CourseType Enum
    /// </summary>
    public enum CourseType
    {
        Book,
        Online,
        LunchAndLearn,
        Audio
    }
}
