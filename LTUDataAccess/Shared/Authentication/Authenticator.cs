﻿using LTUCommon.Interfaces.Providers;

namespace LTUDataAccess.Shared.Authentication
{
    public abstract class Authenticator
    {
        protected readonly ICurrentUserInfo currentUserInfo;

        /// <summary>
        /// Parent Authenticate user - authenticat with db
        /// </summary>
        /// <param name="currentUserInfo"></param>
        public Authenticator
        (
            ICurrentUserInfo currentUserInfo
        )
        {
            this.currentUserInfo = currentUserInfo;
        }
    }
}
