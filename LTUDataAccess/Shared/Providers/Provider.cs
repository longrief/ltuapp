﻿using LTUCommon.Interfaces.Providers;
using LTUDataAccess.Shared.Authentication;

namespace LTUDataAccess.Shared.Providers
{
    public abstract class Provider: Authenticator
    {
        /// <summary>
        /// Parent Provider
        /// </summary>
        public Provider(ICurrentUserInfo currentUserInfo) : base(currentUserInfo)
        {
        }
    }
}
