﻿using LTUCommon.Interfaces.Providers;
using LTUDataAccess.Shared.Authentication;

namespace LTUDataAccess.Shared.Depositors
{
    public abstract class Depositor: Authenticator
    {
        /// <summary>
        /// Parent Depositor
        /// </summary>
        public Depositor(ICurrentUserInfo currentUserInfo) : base(currentUserInfo)
        {
        }
    }
}
