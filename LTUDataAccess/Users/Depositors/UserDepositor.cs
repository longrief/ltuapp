﻿using LTUCommon.Interfaces.Providers;
using LTUDataAccess.Shared.Depositors;
using LTUDataAccess.Users.Utils;
using LTUEntities.Users;
using System;

namespace LTUDataAccess.Users.Depositors
{
    public class UserDepositor : Depositor, LTUEntities.Users.Interfaces.IUserDepositor
    {
        private readonly UserConverter converter;

        /// <summary>
        /// Convert User
        /// </summary>
        /// <param name="currentUserInfo"></param>
        public UserDepositor(ICurrentUserInfo currentUserInfo) : base(currentUserInfo)
        {
            this.converter = new UserConverter();
        }

        /// <summary>
        /// Deposit User into db
        /// </summary>
        /// <param name="user"></param>
        public void Deposit(ref User user)
        {
            throw new NotImplementedException();
        }
    }
}
