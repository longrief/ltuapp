﻿using LTUDomain.Users;
using LTUEntities.Users;
using System.Collections.Generic;

namespace LTUDataAccess.Users.Utils
{
    public class UserConverter
    {
        /// <summary>
        /// Convert LTUDomain.Users.USER to LTUEntities.Users.User
        /// </summary>
        /// <param name="databaseUser"></param>
        /// <returns></returns>
        public User Convert(USER databaseUser)
        {
            if (databaseUser == null) return null;

            return
                new User()
                {
                    UserId = databaseUser.USERID,
                    Name = databaseUser.NAME,
                    Password = databaseUser.PASSWORD
                };
        }

        /// <summary>
        /// Convert LTUEntities.Users.User to LTUDomain.Users.USER
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public USER Convert(User user)
        {
            if (user == null) return null;

            return
                new USER()
                {
                    USERID = user.UserId,
                    NAME = user.Name,
                    PASSWORD = user.Password
                };
        }
        /// <summary>
        /// LTUDomain.Users IEnumerable<USER> to LTUEntities.Users ICollection<User>
        /// </summary>
        /// <param name="databaseUsers"></param>
        /// <returns></returns>
        public ICollection<User> Convert(IEnumerable<USER> databaseUsers)
        {
            var output = new HashSet<User>();

            foreach (var databaseUser in databaseUsers)
            {
                output
                    .Add
                    (
                        Convert(databaseUser)
                    );
            }

            return output;
        }
    }
}
