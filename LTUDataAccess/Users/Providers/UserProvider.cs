﻿using LTUCommon.Interfaces.Providers;
using LTUDataAccess.Shared.Providers;
using LTUDataAccess.Users.Utils;
using LTUEntities.Authentication.Interfaces;
using LTUEntities.Authentication.Models;
using LTUEntities.Users;
using LTUEntities.Users.Interfaces;
using System;

namespace LTUDataAccess.Users.Providers
{
    public class UserProvider : Provider, IUserProvider
    {
        private readonly UserConverter converter;
        private readonly IUserPasswordEncryptor userPasswordEncryptor;

        /// <summary>
        /// Constructor - converts and encrypts
        /// </summary>
        /// <param name="currentUserInfo"></param>
        /// <param name="userPasswordEncryptor"></param>
        public UserProvider
        (
            ICurrentUserInfo currentUserInfo,
            IUserPasswordEncryptor userPasswordEncryptor
        ) : base(currentUserInfo)
        {
            converter = new UserConverter();
            this.userPasswordEncryptor = userPasswordEncryptor;
        }

        /// <summary>
        /// Get With userId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void With
        (
            string userId,
            Action<User> with,
            Action without = null
        )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get With credentials
        /// </summary>
        /// <param name="credentials"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void WithCredentials
        (
            UserCredentials credentials,
            Action<User> with,
            Action without = null
        )
        {
            throw new NotImplementedException();
        }

    }
}
