﻿using LTUCommon.Interfaces.Providers;
using LTUDataAccess.Shared.Depositors;
using LTUEntities.Courses;
using LTUEntities.Courses.Interfaces;
using System;

namespace LTUDataAccess.Courses.Depositors
{
    public class CourseDepositor: Depositor, ICourseDepositor
    {
        /// <summary>
        /// Contructor
        /// </summary>
        public CourseDepositor(ICurrentUserInfo currentUserInfo) : base(currentUserInfo)
        {
        }

        /// <summary>
        /// Add new course to database
        /// </summary>
        /// <param name="course"></param>
        public void Deposit(Course course)
        {
            throw new NotImplementedException();
        }
    }
}
