﻿using LTUDataAccess.Utils;
using LTUDomain.Courses;
using LTUEntities.Courses;
using System.Collections.Generic;
using System.Linq;

namespace LTUDataAccess.Courses.Utils
{
    public class CourseConverter
    {
        /// <summary>
        /// Converts LTUDomain.COURSE to LTUEntities.Course
        /// </summary>
        /// <param name="databaseCourse"></param>
        /// <returns></returns>
        public Course Convert
        (
            COURSE databaseCourse
        )
        {
            if (databaseCourse == null) return null;

            return
                new Course()
                {
                    Id = databaseCourse.COURSENUMBER ?? 0,
                    Name = databaseCourse.NAME,
                    Type = CourseTypeConverter.Convert(databaseCourse.TYPE),
                    Subject = databaseCourse.SUBJECT,
                    AttendeesNumber = databaseCourse.ATTENDEESNUMBER ?? 0,
                    CompletedNumber = databaseCourse.COURSENUMBER ?? 0,
                    AverageStarRating = databaseCourse.AVERAGESTARRATING ?? 0.0
                };
        }

        /// <summary>
        /// Converts IEnumerable<LTUDomain.COURSE> to IEnumerable<LTUEntities.Courses.Course>
        /// </summary>
        /// <param name="dbCourses"></param>
        /// <returns></returns>
        public IEnumerable<Course> Convert
        (
            IEnumerable<COURSE> dbCourses
        )
        {
            var output = new List<Course>();

            if (dbCourses == null || !dbCourses.Any())
            {
                return output;
            }

            foreach (var dbCourse in dbCourses)
            {
                output
                    .Add
                    (
                        Convert(dbCourse)
                    );
            }

            return output;
        }

        /// <summary>
        /// Converts IEnumerable<LTUEntities.Courses.Course> To IEnumerable<LTUDomain.COURSE>
        /// </summary>
        /// <param name="courses"></param>
        /// <returns></returns>
        public IEnumerable<COURSE> Convert
        (
            IEnumerable<Course> courses
        )
        {
            var output = new List<COURSE>();

            if (courses == null || !courses.Any())
            {
                return output;
            }

            foreach (var course in courses)
            {
                output
                    .Add
                    (
                        Convert(course)
                    );
            }

            return output;
        }
        /// <summary>
        /// Converts LTUEntities.Course to LTUDomain.COURSE
        /// </summary>
        /// <param name="course"></param>
        /// <returns></returns>
        public COURSE Convert
        (
            Course course
        )
        {
            if (course == null) return null;

            return
                new COURSE()
                {
                    COURSENUMBER = course.Id,
                    NAME = course.Name,
                    TYPE = CourseTypeConverter.Convert(course.Type),
                    SUBJECT = course.Subject,
                    ATTENDEESNUMBER = course.AttendeesNumber,
                    COMPLETEDNUMBER = course.CompletedNumber,
                    AVERAGESTARRATING = course.AverageStarRating
                };
        }
    }
}
