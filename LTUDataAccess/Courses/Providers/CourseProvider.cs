﻿using LTUCommon.Interfaces.Providers;
using LTUDataAccess.Shared.Providers;
using LTUEntities.Courses;
using LTUEntities.Courses.Interfaces;
using System;
using System.Collections.Generic;

namespace LTUDataAccess.Courses.Providers
{
    public class CourseProvider : Provider, ICourseProvider
    {
        /// <summary>
        /// Contructor
        /// </summary>
        public CourseProvider(ICurrentUserInfo currentUserInfo) : base(currentUserInfo)
        {
        }

        /// <summary>
        /// Get With courseNumber
        /// </summary>
        /// <param name="id"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void With
        (
            int id, 
            Action<Course> with, 
            Action without = null
        )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get With course name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void WithName
        (
            string name, 
            Action<IEnumerable<Course>> with, 
            Action without = null
        )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get With search phrase
        /// </summary>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <param name="searchPhrase"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void WithSearchPhrase
        (
            int skip, 
            int take, 
            string searchPhrase, 
            Action<IEnumerable<Course>> with, 
            Action without = null
        )
        {
            throw new NotImplementedException();
        }
    }
}
