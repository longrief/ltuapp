﻿using System;

namespace LTUDataAccess.Utils
{
    public class LTUDomainAuthenticator
    {
        /// <summary>
        /// Set user properties
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="domainId"></param>
        /// <param name="systemId"></param>
        /// <param name="password"></param>
        public void SetCurrentPrincipalAndClaims
        (
            string userId,
            string domainId,
            string systemId,
            string password
        )
        {
            SetCurrentPrincipalAndClaims
            (
                userId,
                domainId,
                systemId,
                password,
                useExistingPrincipal: true
            );
        }

        /// <summary>
        /// Reset user properties
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="domainId"></param>
        /// <param name="systemId"></param>
        /// <param name="password"></param>
        public void ResetCurrentPrincipalAndClaims
        (
            string userId,
            string domainId,
            string systemId,
            string password
        )
        {
            SetCurrentPrincipalAndClaims
            (
                userId,
                domainId,
                systemId,
                password,
                useExistingPrincipal: false
            );
        }
        /// <summary>
        /// Set user properties in Domain
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="domainId"></param>
        /// <param name="systemId"></param>
        /// <param name="password"></param>
        /// <param name="useExistingPrincipal"></param>
        private void SetCurrentPrincipalAndClaims
        (
            string userId,
            string domainId,
            string systemId,
            string password,
            bool useExistingPrincipal
        )
        {
            throw new NotImplementedException();
        }
    }
}
