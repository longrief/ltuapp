﻿using LTUWebApp.Models;
using System.Collections.Generic;
using System.Linq;

namespace LTUWebApp.Utils
{
    public class MostEffectiveFormulator
    {
        /// <summary>
        /// Calculates effectiveness and returns most effective
        /// </summary>
        /// <returns></returns>
        public Course GetMostEffective()
        {
            var course =
                new Course();

            var courses =
                new CourseConverter()
                .ConvertCoursesToLTUWebApp();

            var output =
                new List<Course>();

            foreach (var c in courses)
            {
                course = new Course();

                course.Id = c.Id;
                course.Name = c.Name;
                course.Type = (c.Type);
                course.Subject = c.Subject;
                course.AttendeesNumber = c.AttendeesNumber;
                course.CompletedNumber = c.CompletedNumber;
                course.AverageStarRating = c.AverageStarRating;
                course.Effective = c.CompletedNumber / c.AttendeesNumber * c.AverageStarRating;

                output.Add(course);
            }

            var mostEffective =
                output
                .OrderByDescending(x => x.Effective)
                .FirstOrDefault();

            return
                mostEffective;
        }
    }
}
