﻿using LTUWebApp.Models;
using System.Collections.Generic;
using System.Linq;

namespace LTUWebApp.Utils
{
    public class MostPopularFormulator
    {
        /// <summary>
        /// Calculates popularity and returns most popular
        /// </summary>
        /// <returns></returns>
        public Course GetMostPopular()
        {
            var course =
                new Course();

            var courses =
                new CourseConverter()
                .ConvertCoursesToLTUWebApp();

            var output =
                new List<Course>();

            foreach (var c in courses)
            {
                course = new Course();

                course.Id = c.Id;
                course.Name = c.Name;
                course.Type = (c.Type);
                course.Subject = c.Subject;
                course.AttendeesNumber = c.AttendeesNumber;
                course.CompletedNumber = c.CompletedNumber;
                course.AverageStarRating = c.AverageStarRating;
                course.Popularity = c.AttendeesNumber + c.AverageStarRating;

                output.Add(course);
            }

            var mostPopular =
                output
                .OrderByDescending(x => x.Popularity)
                .FirstOrDefault();

            return
                mostPopular;
        }
    }
}
