﻿using LTUDataAccess.Mocks.Courses.Providers;
using LTUDataAccess.Utils;
using LTUWebApp.Models;
using System.Collections.Generic;

namespace LTUWebApp.Utils
{
    public class CourseConverter
    {
        /// <summary>
        /// Converts Courses from LTUDataAccess Courses to LTUWebApp Courses
        /// </summary>
        /// <returns></returns>
        public List<Course> ConvertCoursesToLTUWebApp()
        {
            var courses =
                CourseProvider.Courses;

            var course = new Course();

            var output =
                new List<Course>();

            foreach (var c in courses)
            {
                course = new Course();

                course.Id = c.Id;
                course.Name = c.Name;
                course.Type = CourseTypeConverter.Convert(c.Type);
                course.Subject = c.Subject;
                course.AttendeesNumber = c.AttendeesNumber;
                course.CompletedNumber = c.CompletedNumber;
                course.AverageStarRating = c.AverageStarRating;

                output.Add(course);

            }
            return output;
        }

        /// <summary>
        /// Converts Course from LTUWebApp Course to LTUEntities Course
        /// </summary>
        /// <param name="course"></param>
        /// <returns></returns>
        public LTUEntities.Courses.Course ConvertCoursesToLTUEntities
        (
            LTUWebApp.Models.Course course
        )
        {

            var courseEntities = 
                new LTUEntities
                    .Courses
                    .Course();

                courseEntities.Id = course.Id;
                courseEntities.Name = course.Name;
                courseEntities.Type = CourseTypeConverter.Convert(course.Type);
                courseEntities.Subject = course.Subject;
                courseEntities.AttendeesNumber = course.AttendeesNumber;
                courseEntities.CompletedNumber = course.CompletedNumber;
                courseEntities.AverageStarRating = course.AverageStarRating;

            return courseEntities;
        }
    }
}
