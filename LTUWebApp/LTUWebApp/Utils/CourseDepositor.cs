﻿using LTUEntities.Courses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LTUWebApp.Utils
{
    public class CourseDepositor
    {
        /// <summary>
        /// Adds course to DB
        /// </summary>
        /// <param name="course"></param>
        public void Deposit
        (
            Course course
        )
        {
            new
                LTUDataAccess
                .Mocks
                .Courses
                .Depositors
                .CourseDepositor()
                .Deposit
                (
                    course
                );
        }
    }
}
