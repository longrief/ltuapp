﻿using System.ComponentModel.DataAnnotations;

namespace LTUWebApp.Models
{
    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Subject { get; set; }
        [Range(1, int.MaxValue)]
        public int AttendeesNumber { get; set; }
        [Range(1, int.MaxValue)]
        public int CompletedNumber { get; set; }
        [Range(0.1, double.MaxValue)]
        public double AverageStarRating { get; set; }
        public double Popularity { get; set; }
        public double Effective { get; set; }

    }
}
