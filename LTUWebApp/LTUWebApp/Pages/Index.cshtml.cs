﻿using System.Collections.Generic;
using System.Linq;
using LTUWebApp.Models;
using LTUWebApp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace LTUWebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        [BindProperty(SupportsGet = true)]
        public Course Course { get; set; }
        [BindProperty(SupportsGet = true)]
        public IEnumerable<Course> Courses { get; set; }

        [BindProperty(SupportsGet = true)]
        public int NextCourseId { get; set; }
        [BindProperty(SupportsGet = true)]
        public bool ShowMostPopular { get; set; } = false;
        [BindProperty(SupportsGet = true)]
        public bool ShowMostEffective { get; set; } = false;
        [BindProperty(SupportsGet = true)]
        public bool ShowAddCourse { get; set; } = false;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Standard Get Method - Sets List<Courses> 
        /// </summary>
        public void OnGet()
        {
            SetCourses();
        }

        /// <summary>
        /// Standard Post Method
        /// </summary>
        public void OnPost()
        {

        }
        /// <summary>
        /// Sets most popular 
        /// Shows/hides divs most popular/most effective & add course
        /// </summary>
        public void OnPostMostPopular()
        {
            var mostPopular = 
                new MostPopularFormulator()
                .GetMostPopular();

            Course = mostPopular;

            ShowMostEffective = false;
            ShowMostPopular = true;
            ShowAddCourse = false;
            SetCourses();

        }
        /// <summary>
        /// Sets most effective 
        /// Shows/hides divs most effective/most popular & add course
        /// </summary>
        public void OnPostMostEffective()
        {
            var mostEffective = 
                new MostEffectiveFormulator()
                .GetMostEffective();

            Course = mostEffective;

            ShowMostEffective = true;
            ShowMostPopular = false;
            ShowAddCourse = false;
            SetCourses();

        }
        /// <summary>
        /// Shows/hides divs most effective & most popular/add course
        /// </summary>
        public void OnPostShowAddCourseCard()
        {
            ShowMostEffective = false;
            ShowMostPopular = false;
            ShowAddCourse = true;
            SetCourses();
            NextCourseId =
                Courses.Count() + 1;
        }

        /// <summary>
        /// Adds new Course to Courses
        /// Shows/hides divs most effective/most popular/add course
        /// </summary>
        public void OnPostAddCourse()
        {
            ShowMostEffective = false;
            ShowMostPopular = false;
            ShowAddCourse = false;
            SetCourses();

            var coursesTemp = 
                new List<Course>();

            coursesTemp = 
                Courses
                    .ToList();

            coursesTemp
                .Add(Course);

            Courses = coursesTemp;

            AddCourseToDB(Course);
        }

        /// <summary>
        /// Method to set Coursess
        /// </summary>
        private void SetCourses()
        {
            Courses =
                new CourseConverter()
                .ConvertCoursesToLTUWebApp();
        }

        /// <summary>
        /// Convert Course To LTUEntities
        /// Add Course to DB
        /// </summary>
        /// <param name="course"></param>
        private void AddCourseToDB
        (
            Course course
        )
        {
            var courseToAdd = 
                new CourseConverter()
                .ConvertCoursesToLTUEntities
                (
                    course
                );

            new CourseDepositor()
                .Deposit
                (
                    courseToAdd
                );
        }
    }
}
