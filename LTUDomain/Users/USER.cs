﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security;
using System.Text;

namespace LTUDomain.Users
{
    public class USER
    {
        [StringLength(50), Required]
        public string USERID { get; set; }
        [StringLength(50)]
        public string NAME { get; set; }
        [StringLength(50)]
        public string PASSWORD { get; set; }
    }
}
