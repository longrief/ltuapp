﻿using System.ComponentModel.DataAnnotations;

namespace LTUDomain.Courses
{
    public class COURSE
    {
        public COURSE()
        {}
        public int? COURSENUMBER { get; set; }
        [StringLength(50)]
        public string NAME { get; set; }
        [StringLength(50)]
        public string TYPE { get; set; }
        [StringLength(50)]
        public string SUBJECT { get; set; }
        public int? ATTENDEESNUMBER { get; set; }
        public int? COMPLETEDNUMBER { get; set; }
        public double? AVERAGESTARRATING { get; set; }
    }
}
