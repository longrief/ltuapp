using LTUWebApp.NUnit.Tests.Helpers;
using LTUWebApp.Utils;
using NUnit.Framework;
using System.Linq;

namespace LTUWebApp.NUnit.Tests.Utils
{
    public class CourseConverterTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestConvertCoursesToLTUWebApp()
        {
            var courses =
                CourseProvider.Courses;

            var cors =
                new CourseConverter()
                    .ConvertCoursesToLTUWebApp();

            for (int i = 0; i < courses.ToList().Count; i++)
            {
                var course = courses.ToList()[i];
                var cor = cors.ToList()[i];

                Assert.AreEqual(cor.Id, course.Id);
                Assert.AreEqual(cor.Name, course.Name);
                Assert.AreEqual(cor.Type, course.Type.ToString());
                Assert.AreEqual(cor.AttendeesNumber, course.AttendeesNumber);
                Assert.AreEqual(cor.CompletedNumber, course.CompletedNumber);
                Assert.AreEqual(cor.AverageStarRating, course.AverageStarRating);
            }
        }

        [Test]
        public void TestConvertCoursesToLTUEntities()
        {
            var course =
                CourseProvider
                .Courses
                .ToList()
                .FirstOrDefault();

            var courseEntities =
                new LTUEntities
                    .Courses
                    .Course()
                {
                    Id = 1,
                    Name = "Agile Methodologies",
                    Type = CourseTypeConverter.Convert("Book"),
                    Subject = "Principles",
                    AttendeesNumber = 26,
                    CompletedNumber = 24,
                    AverageStarRating = 4.5
                };

            Assert.AreEqual(course.Id, courseEntities.Id);
            Assert.AreEqual(course.Name, courseEntities.Name);
            Assert.AreEqual(course.Type, courseEntities.Type);
            Assert.AreEqual(course.Subject, courseEntities.Subject);
            Assert.AreEqual(course.AttendeesNumber, courseEntities.AttendeesNumber);
            Assert.AreEqual(course.CompletedNumber, courseEntities.CompletedNumber);
            Assert.AreEqual(course.AverageStarRating, courseEntities.AverageStarRating);


        }
    }
}