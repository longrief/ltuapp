using LTUWebApp.Models;
using LTUWebApp.NUnit.Tests.Helpers;
using LTUWebApp.Utils;
using NUnit.Framework;
using System.Linq;

namespace LTUWebApp.NUnit.Tests.Utils
{
    public class MostEffectiveFormulatorTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestGetMostEffective()
        {
            var courseResult =
                new LTUWebApp.Utils
                    .MostEffectiveFormulator()
                    .GetMostEffective();

            var course =
                new Course()
                {
                    Id = 4,
                    Name = "Unit Test in Applications",
                    Type = "LunchAndLearn",
                    Subject = "Code",
                    AttendeesNumber = 18,
                    CompletedNumber = 18,
                    AverageStarRating = 4,
                    Popularity = 0,
                    Effective = 4
                };

            Assert.AreEqual(course.Id, courseResult.Id);
            Assert.AreEqual(course.Name, courseResult.Name);
            Assert.AreEqual(course.Type, courseResult.Type);
            Assert.AreEqual(course.Subject, courseResult.Subject);
            Assert.AreEqual(course.AttendeesNumber, courseResult.AttendeesNumber);
            Assert.AreEqual(course.CompletedNumber, courseResult.CompletedNumber);
            Assert.AreEqual(course.AverageStarRating, courseResult.AverageStarRating);
            Assert.AreEqual(course.Popularity, courseResult.Popularity);
            Assert.AreEqual(course.Effective, courseResult.Effective);
        }
    }
}