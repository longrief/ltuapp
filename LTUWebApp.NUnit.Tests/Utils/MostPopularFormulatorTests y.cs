using LTUWebApp.Models;
using LTUWebApp.NUnit.Tests.Helpers;
using LTUWebApp.Utils;
using NUnit.Framework;
using System.Linq;

namespace LTUWebApp.NUnit.Tests.Utils
{
    public class MostPopularFormulatorTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestGetMostPopular()
        {
            var courseResult =
                new LTUWebApp.Utils
                    .MostPopularFormulator()
                    .GetMostPopular();

            var course =
                new Course()
                {
                    Id = 1,
                    Name = "Agile Methodologies",
                    Type = "Book",
                    Subject = "Principles",
                    AttendeesNumber = 26,
                    CompletedNumber = 24,
                    AverageStarRating = 4.5,
                    Popularity = 30.5,
                    Effective = 0
                };

            Assert.AreEqual(course.Id, courseResult.Id);
            Assert.AreEqual(course.Name, courseResult.Name);
            Assert.AreEqual(course.Type, courseResult.Type);
            Assert.AreEqual(course.Subject, courseResult.Subject);
            Assert.AreEqual(course.AttendeesNumber, courseResult.AttendeesNumber);
            Assert.AreEqual(course.CompletedNumber, courseResult.CompletedNumber);
            Assert.AreEqual(course.AverageStarRating, courseResult.AverageStarRating);
            Assert.AreEqual(course.Popularity, courseResult.Popularity);
            Assert.AreEqual(course.Effective, courseResult.Effective);
        }
    }
}