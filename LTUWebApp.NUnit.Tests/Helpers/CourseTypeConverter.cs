﻿using LTUCommon.Enums;

namespace LTUWebApp.NUnit.Tests.Helpers
{
    public class CourseTypeConverter
    {
        // <summary>
        /// Convert LTUDomain.Courses.COURSE.TYPE<string> to LTUCommon.Enums.CouresType
        /// </summary>
        /// <param name="courseType"></param>
        /// <returns></returns>
        public static CourseType Convert(string courseType)
        {
            switch (courseType)
            {
                case "Book":
                    return CourseType.Book;
                case "Online":
                    return CourseType.Online;
                case "LunchAndLearn":
                    return CourseType.LunchAndLearn;
                case "Audio":
                    return CourseType.Audio;
                default:
                    return CourseType.Book;
            }
        }
        /// <summary>
        /// Convert LTUCommon.Enums.CouresType to LTUDomain.Courses.COURSE.TYPE<string>
        /// </summary>
        /// <param name="courseType"></param>
        /// <returns></returns>
        public static string Convert(CourseType courseType)
        {
            switch (courseType)
            {
                case CourseType.Book:
                    return "Book";
                case CourseType.Online:
                    return "Online";
                case CourseType.LunchAndLearn:
                    return "LunchAndLearn";
                case CourseType.Audio:
                    return "Audio";
                default:
                    return "Book";
            }
        }
    }
}
