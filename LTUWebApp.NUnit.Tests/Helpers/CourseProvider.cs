﻿using LTUDomain.Courses;
using LTUEntities.Courses;
using System;
using System.Collections.Generic;
using System.Text;

namespace LTUWebApp.NUnit.Tests.Helpers
{
    public class CourseProvider
    {
        /// <summary>
        /// Mock data list of Course Objects
        /// </summary>
        public static List<Course> Courses =
            new List<Course>()
            {
                new Course()
                {
                    Id = 1,
                    Name = "Agile Methodologies",
                    Type = LTUCommon.Enums.CourseType.Book,
                    Subject = "Principles",
                    AttendeesNumber = 26,
                    CompletedNumber = 24,
                    AverageStarRating = 4.5
                },

                new Course()
                {
                    Id = 2,
                    Name = "Clean Code",
                    Type = LTUCommon.Enums.CourseType.Book,
                    Subject = "Code",
                    AttendeesNumber = 19,
                    CompletedNumber = 17,
                    AverageStarRating = 4.5
                },

                new Course()
                {
                    Id = 3,
                    Name = "C# language",
                    Type = LTUCommon.Enums.CourseType.Online,
                    Subject = "Code",
                    AttendeesNumber = 14,
                    CompletedNumber = 11,
                    AverageStarRating = 3
                },

                new Course()
                {
                    Id = 4,
                    Name = "Unit Test in Applications",
                    Type = LTUCommon.Enums.CourseType.LunchAndLearn,
                    Subject = "Code",
                    AttendeesNumber = 18,
                    CompletedNumber = 18,
                    AverageStarRating = 4
                },

                new Course()
                {
                    Id = 5,
                    Name = "Design Patterns",
                    Type = LTUCommon.Enums.CourseType.Book,
                    Subject = "Methods",
                    AttendeesNumber = 13,
                    CompletedNumber = 6,
                    AverageStarRating = 3.5
                }
            };

        /// <summary>
        /// Mock data list of COURSE Objects
        /// </summary>
        public static List<COURSE> COURSES =
            new List<COURSE>()
            {
                new COURSE()
                {
                    COURSENUMBER = 1,
                    NAME = "Agile Methodologies",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.Book),
                    SUBJECT = "Principles",
                    ATTENDEESNUMBER = 26,
                    COMPLETEDNUMBER = 24,
                    AVERAGESTARRATING = 4.5
                },

                new COURSE()
                {
                    COURSENUMBER = 2,
                    NAME = "Clean Code",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.Book),
                    SUBJECT = "Code",
                    ATTENDEESNUMBER = 19,
                    COMPLETEDNUMBER = 17,
                    AVERAGESTARRATING = 4.5
                },

                new COURSE()
                {
                    COURSENUMBER = 3,
                    NAME = "C# language",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.Online),
                    SUBJECT = "Code",
                    ATTENDEESNUMBER = 14,
                    COMPLETEDNUMBER = 11,
                    AVERAGESTARRATING = 3
                },

                new COURSE()
                {
                    COURSENUMBER = 4,
                    NAME = "Unit Test in Applications",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.LunchAndLearn),
                    SUBJECT = "Code",
                    ATTENDEESNUMBER = 18,
                    COMPLETEDNUMBER = 18,
                    AVERAGESTARRATING = 4
                },

                new COURSE()
                {
                    COURSENUMBER = 5,
                    NAME = "Design Patterns",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.Book),
                    SUBJECT = "Methods",
                    ATTENDEESNUMBER = 13,
                    COMPLETEDNUMBER = 6,
                    AVERAGESTARRATING = 3.5
                }
            };
    }
}
