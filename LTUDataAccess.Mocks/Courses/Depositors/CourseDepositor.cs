﻿using LTUDataAccess.Mocks.Courses.Providers;
using LTUEntities.Courses;
using LTUEntities.Courses.Interfaces;
using System.Linq;

namespace LTUDataAccess.Mocks.Courses.Depositors
{
    public class CourseDepositor: ICourseDepositor
    {
        /// <summary>
        /// Deposits Course in mock data 
        /// </summary>
        /// <param name="course"></param>
        public void Deposit(Course course)
        {
            if
            (
                !CourseProvider
                    .Courses
                    .Any
                    (
                        c =>
                            c.Id == course.Id
                    )
            )
                CourseProvider.Courses.Add(course);
            else
            {
                var index =
                    CourseProvider
                        .Courses
                        .IndexOf
                        (
                            CourseProvider
                                .Courses
                                .First
                                (
                                    c =>
                                        c.Id == course.Id
                                )
                        );

                CourseProvider.Courses[index] = course;
            }
        }
    }
}
