﻿using LTUDataAccess.Utils;
using LTUDomain.Courses;
using LTUEntities.Courses;
using LTUEntities.Courses.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LTUDataAccess.Mocks.Courses.Providers
{
    public class CourseProvider : ICourseProvider
    {
        /// <summary>
        /// Mock data list of Course Objects
        /// </summary>
        public static List<Course> Courses =
            new List<Course>()
            {
                new Course()
                {
                    Id = 1,
                    Name = "Agile Methodologies",
                    Type = LTUCommon.Enums.CourseType.Book,
                    Subject = "Principles",
                    AttendeesNumber = 26,
                    CompletedNumber = 24,
                    AverageStarRating = 4.5
                },

                new Course()
                {
                    Id = 2,
                    Name = "Clean Code",
                    Type = LTUCommon.Enums.CourseType.Book,
                    Subject = "Code",
                    AttendeesNumber = 19,
                    CompletedNumber = 17,
                    AverageStarRating = 4.5
                },

                new Course()
                {
                    Id = 3,
                    Name = "C# language",
                    Type = LTUCommon.Enums.CourseType.Online,
                    Subject = "Code",
                    AttendeesNumber = 14,
                    CompletedNumber = 11,
                    AverageStarRating = 3
                },

                new Course()
                {
                    Id = 4,
                    Name = "Unit Test in Applications",
                    Type = LTUCommon.Enums.CourseType.LunchAndLearn,
                    Subject = "Code",
                    AttendeesNumber = 18,
                    CompletedNumber = 18,
                    AverageStarRating = 4
                },

                new Course()
                {
                    Id = 5,
                    Name = "Design Patterns",
                    Type = LTUCommon.Enums.CourseType.Book,
                    Subject = "Methods",
                    AttendeesNumber = 13,
                    CompletedNumber = 6,
                    AverageStarRating = 3.5
                }
            };

        /// <summary>
        /// Mock data list of COURSE Objects
        /// </summary>
        public static List<COURSE> COURSES =
            new List<COURSE>()
            {
                new COURSE()
                {
                    COURSENUMBER = 1,
                    NAME = "Agile Methodologies",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.Book),
                    SUBJECT = "Principles",
                    ATTENDEESNUMBER = 26,
                    COMPLETEDNUMBER = 24,
                    AVERAGESTARRATING = 4.5
                },

                new COURSE()
                {
                    COURSENUMBER = 2,
                    NAME = "Clean Code",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.Book),
                    SUBJECT = "Code",
                    ATTENDEESNUMBER = 19,
                    COMPLETEDNUMBER = 17,
                    AVERAGESTARRATING = 4.5
                },

                new COURSE()
                {
                    COURSENUMBER = 3,
                    NAME = "C# language",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.Online),
                    SUBJECT = "Code",
                    ATTENDEESNUMBER = 14,
                    COMPLETEDNUMBER = 11,
                    AVERAGESTARRATING = 3
                },

                new COURSE()
                {
                    COURSENUMBER = 4,
                    NAME = "Unit Test in Applications",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.LunchAndLearn),
                    SUBJECT = "Code",
                    ATTENDEESNUMBER = 18,
                    COMPLETEDNUMBER = 18,
                    AVERAGESTARRATING = 4
                },

                new COURSE()
                {
                    COURSENUMBER = 5,
                    NAME = "Design Patterns",
                    TYPE = CourseTypeConverter.Convert(LTUCommon.Enums.CourseType.Book),
                    SUBJECT = "Methods",
                    ATTENDEESNUMBER = 13,
                    COMPLETEDNUMBER = 6,
                    AVERAGESTARRATING = 3.5
                }
            };

        /// <summary>
        /// Get With courseNumber
        /// </summary>
        /// <param name="courseNumber"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void With
        (
            int courseNumber,
            Action<Course> with,
            Action without = null
        )
        {
            var course =
                Courses
                    .FirstOrDefault
                    (
                        crs =>
                            crs.Id == courseNumber
                    );

            if (course != null)
                with(course);
            else if (!(without == null))
                without();
        }

        /// <summary>
        /// Get With Name
        /// </summary>
        /// <param name="name"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void WithName
        (
            string name,
            Action<IEnumerable<Course>> with,
            Action without = null
        )
        {
            var foundCourses =
                Courses
                    .Where
                    (
                        crs =>
                            crs.Name == name
                    );

            if (foundCourses.Any())
                with(foundCourses);
            else if (!(without == null))
                without();
        }

        /// <summary>
        /// Get With searchPhrase
        /// </summary>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <param name="searchPhrase"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void WithSearchPhrase
        (
            int skip,
            int take,
            string searchPhrase,
            Action<IEnumerable<Course>> with,
            Action without = null
        )
        {
            var foundCourses =
                 Courses
                 .Where
                     (
                        course =>
                        {
                            if (!String.IsNullOrWhiteSpace(searchPhrase))
                            {
                                int id = 0;
                                Int32.TryParse(searchPhrase, out id);


                                bool searchPhraseMatch =
                                    (
                                        (id != 0 && (course.Id) == id) ||
                                        (course.Name ?? "").Contains(searchPhrase) ||
                                        (course.Type.ToString() ?? "").Contains(searchPhrase) ||
                                        (course.Subject ?? "").Contains(searchPhrase) ||
                                        (course.AttendeesNumber.ToString() ?? "").Contains(searchPhrase) ||
                                        (course.CompletedNumber.ToString() ?? "").Contains(searchPhrase) ||
                                        (course.AverageStarRating.ToString() ?? "").Contains(searchPhrase)
                                    );


                                return (searchPhraseMatch);
                            }

                            return false;
                        }
                    )
                    .ToList()
                    .Skip(skip)
                    .Take(take);

            if (foundCourses.Any())
                with(foundCourses);
            else if (!(without == null))
                without();
        }
    }
}
