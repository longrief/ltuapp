﻿using LTUCommon.Interfaces.Logging;
using LTUCommon.Interfaces.Providers;
using Newtonsoft.Json;
using System;
using System.IO;

namespace LTUDataAccess.Mocks.Logging
{
    public class Logger : ILogger
    {
        private readonly ICurrentUserInfo currentUserInfo;
        private readonly string logFilePath;

        /// <summary>
        /// Logger constructor
        /// </summary>
        /// <param name="currentUserInfo"></param>
        public Logger(ICurrentUserInfo currentUserInfo)
        {
            this.currentUserInfo = currentUserInfo;

            DateTime today = DateTime.Now.Date;
            string directoryPath = "LTUCourse_app_logs";
            //string directoryPath = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "ServiceRequestAppLogs";
            string logFileName = "log " + today.Year.ToString() + "-" + today.Month.ToString() + "-" + today.Day.ToString() + ".txt";

            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            logFilePath = Path.Combine(directoryPath, logFileName);
        }

        /// <summary>
        /// Write message to log file
        /// </summary>
        /// <param name="message"></param>
        public void Log(string message)
        {
            WriteToLogFile(message);
        }

        /// <summary>
        /// Write message and object to log file
        /// </summary>
        /// <param name="message"></param>
        /// <param name="obj"></param>
        public void Log(string message, object obj)
        {
            WriteToLogFile
            (
                message +
                Environment.NewLine +
                JsonConvert.SerializeObject(obj)
            );
        }

        /// <summary>
        /// Write message and exception to log file
        /// </summary>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public void Log(string message, Exception exception)
        {
            WriteToLogFile
            (
                message +
                Environment.NewLine +
                Environment.NewLine +
                exception.Message +
                Environment.NewLine +
                Environment.NewLine +
                exception.StackTrace
            );
        }

        /// <summary>
        /// Write message and duration to log file
        /// </summary>
        /// <param name="message"></param>
        /// <param name="duration"></param>
        public void Log(string message, TimeSpan duration)
        {
            WriteToLogFile
            (
                "Duration: " + duration.TotalMilliseconds.ToString() +
                Environment.NewLine +
                message
            );
        }

        /// <summary>
        /// Write message, duration and exception to log file
        /// </summary>
        /// <param name="message"></param>
        /// <param name="duration"></param>
        /// <param name="exception"></param>
        public void Log(string message, TimeSpan duration, Exception exception)
        {
            WriteToLogFile
             (
                 "Duration: " + duration.TotalMilliseconds.ToString() +
                 Environment.NewLine +
                 message +
                 Environment.NewLine +
                 Environment.NewLine +
                 exception.Message +
                 Environment.NewLine +
                 Environment.NewLine +
                 exception.StackTrace
             );
        }

        /// <summary>
        /// Write message, duration and object to log file
        /// </summary>
        /// <param name="message"></param>
        /// <param name="duration"></param>
        /// <param name="obj"></param>
        public void Log(string message, TimeSpan duration, object obj)
        {
            WriteToLogFile
            (
                "Duration: " + duration.TotalMilliseconds.ToString() +
                Environment.NewLine +
                message +
                Environment.NewLine +
                JsonConvert.SerializeObject(obj)
            );
        }

        /// <summary>
        /// Write message and duration to log file
        /// </summary>
        /// <param name="text"></param>
        private void WriteToLogFile(string text)
        {
            string userId = currentUserInfo.GetUserId();

            File
                .AppendAllText
                (
                    logFilePath,
                    DateTime.Now +
                    Environment.NewLine +
                    (String.IsNullOrWhiteSpace(userId) ? "" : "User ID: " + userId + Environment.NewLine) +
                    Environment.NewLine +
                    text +
                    Environment.NewLine +
                    Environment.NewLine
                );
        }
    }
}
