﻿using LTUEntities.Authentication.Models;
using LTUEntities.Users;
using LTUEntities.Users.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace LTUDataAccess.Mocks.Users.Providers
{
    public class UserProvider : IUserProvider
    {
        public static List<User> Users = GetUsers();

        /// <summary>
        /// With userId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void With
        (
            string userId,
            Action<User> with,
            Action without = null
        )
        {
            var user =
                Users
                    .FirstOrDefault
                    (
                        u =>
                            u.UserId == userId.ToUpper()
                    );

            if (user != null)
                with(user);
            else if (!(without == null))
                without();

        }

        /// <summary>
        /// With user crredentials
        /// </summary>
        /// <param name="credentials"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        public void WithCredentials
        (
            UserCredentials credentials,
            Action<User> with,
            Action without = null
        )
        {

            var user =
                Users
                    .FirstOrDefault
                    (
                        u =>
                            u.UserId == credentials.UserId &&
                            u.Password == new NetworkCredential(string.Empty, credentials.SecurePassword).Password
                    );

            if (user != null)
                with(user);
            else if (!(without == null))
                without();
        }

        /// <summary>
        /// List of Users mock data
        /// </summary>
        /// <returns></returns>
        private static List<User> GetUsers()
        {
            return
                new List<User>()
                {
                    new User()
                    {
                        UserId = "User",
                        Password = "Password"
                    },
                    new User()
                    {
                        UserId = "Test",
                        Password = "Test"
                    }
                };
        }
    }
}
