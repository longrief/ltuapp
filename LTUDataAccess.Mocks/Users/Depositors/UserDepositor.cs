﻿using LTUDataAccess.Mocks.Users.Providers;
using LTUEntities.Users;
using LTUEntities.Users.Interfaces;
using System.Linq;

namespace LTUDataAccess.Mocks.Users.Depositors
{
    public class UserDepositor : IUserDepositor
    {
        /// <summary>
        /// Deposit User in mock data
        /// </summary>
        /// <param name="user"></param>
        public void Deposit(ref User user)
        {
            var localUser = user;
            if
            (
                !UserProvider
                    .Users
                    .Any
                    (
                        u =>
                            u.UserId == localUser.UserId
                    )
            )
                UserProvider.Users.Add(user);
            else
            {
                var index =
                    UserProvider
                        .Users
                        .IndexOf
                        (
                            UserProvider
                                .Users
                                .First
                                (
                                    u =>
                                        u.UserId == localUser.UserId
                                )
                        );

                UserProvider.Users[index] = user;
            }
        }
    }
}
