﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LTUEntities.Courses.Interfaces
{
    /// <summary>
    /// ICourseProvider Interface
    /// </summary>
    public interface ICourseProvider
    {
        /// <summary>
        /// Get With courseNumber
        /// </summary>
        /// <param name="courseNumber"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        void With
        (
            int courseNumber,
            Action<Course> with,
            Action without = null
        );

        /// <summary>
        /// Get WithName
        /// </summary>
        /// <param name="name"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        void WithName
        (
            string name,
            Action<IEnumerable<Course>> with,
            Action without = null
        );

        /// <summary>
        /// Get With WithSearchPhrase
        /// </summary>
        /// <param name="skip"></param>
        /// <param name="take"></param>
        /// <param name="searchPhrase"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        void WithSearchPhrase
        (
            int skip,
            int take,
            string searchPhrase,
            Action<IEnumerable<Course>> with,
            Action without = null
        );
    }
}
