﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LTUEntities.Courses.Interfaces
{
    /// <summary>
    /// ICourseDepositor Interface
    /// </summary>
    public interface ICourseDepositor
    {
        void Deposit(Course course);
    }
}
