﻿using LTUCommon.Enums;

namespace LTUEntities.Courses
{
    public class Course
    {
        public Course()
        { }
        public int Id { get; set; }
        public string Name { get; set; }
        public CourseType Type { get; set; }
        public string Subject { get; set; }
        public int AttendeesNumber { get; set; }
        public int CompletedNumber { get; set; }
        public double AverageStarRating { get; set; }
    }
}
