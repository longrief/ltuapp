﻿namespace LTUEntities.Users.Interfaces
{
    /// <summary>
    /// IUserDepositor Interface
    /// </summary>
    public interface IUserDepositor
    {
        /// <summary>
        /// Deposit User
        /// </summary>
        /// <param name="user"></param>
        void Deposit(ref User user);
    }
}
