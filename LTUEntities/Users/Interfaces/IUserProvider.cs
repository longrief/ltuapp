﻿using LTUEntities.Authentication.Models;
using System;

namespace LTUEntities.Users.Interfaces
{
    /// <summary>
    /// IUserProvider Interface
    /// </summary>
    public interface IUserProvider
    {
        /// <summary>
        /// With UserId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        void With
        (
            string userId,
            Action<User> with,
            Action without = null
        );

        /// <summary>
        /// With User credentials
        /// </summary>
        /// <param name="credentials"></param>
        /// <param name="with"></param>
        /// <param name="without"></param>
        void WithCredentials
        (
            UserCredentials credentials,
            Action<User> with,
            Action without = null
        );
    }
}
