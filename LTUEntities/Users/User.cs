﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;

namespace LTUEntities.Users
{
    public class User
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
