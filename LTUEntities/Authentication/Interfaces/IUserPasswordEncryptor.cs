﻿namespace LTUEntities.Authentication.Interfaces
{
    /// <summary>
    /// IUserPasswordEncryptor Interface
    /// </summary>
    public interface IUserPasswordEncryptor
    {
        string Encrypt(string password);
    }
}
