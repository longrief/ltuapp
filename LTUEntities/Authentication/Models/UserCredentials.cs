﻿using System;
using System.Net;
using System.Security;

namespace LTUEntities.Authentication.Models
{
    public class UserCredentials
    {
        public string UserId { get; private set; }
        public SecureString SecurePassword { get; private set; }

        public UserCredentials
        (
            string userId,
            SecureString securePassword
        )
        {
            if (string.IsNullOrWhiteSpace(userId))
                RaiseConstructorException("userId");

            if (string.IsNullOrWhiteSpace(new NetworkCredential(string.Empty, securePassword).Password))
                RaiseConstructorException("securePassword");

            UserId = userId.ToUpper();
            SecurePassword = securePassword;
        }

        private void RaiseConstructorException(string requiredPropertyName)
        {
            throw
                new ApplicationException
                (
                    $"Cannot create instance of { this.GetType().Name } with null or empty { requiredPropertyName }."
                );
        }
    }
}
